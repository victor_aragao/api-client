const db = require("../models");
const User = db.users;
const Op = db.Sequelize.Op;
const argon2 = require('argon2');

// Criar e salvar novo usuário
exports.create = (req, res) => {
    // validação request
    if (!req.body.name) {
        res.status(400).send({
            message: "Error"
        });
        return;
    }

    const user = {
        name: req.body.name,
        last_name: req.body.last_name,
        birthday: req.body.birthday,
        email: req.body.email,
        fone: req.body.fone,
        user_access: req.body.user_access,
        password: req.body.password
    };

    // Salvar cliente no banco de dados
    User.create(user)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Não possível cadastrar usuário."
            });
        });
};

// Listar todos usuário do banco de dados
exports.findAll = (req, res) => {

    const name = req.query.name;
    var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;

    User.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Não foi possível listar todos os usuários"
            });
        });
};

// Listar clientes pelo ID
exports.findOne = (req, res) => {
    const id = req.params.id;

    User.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Não foi possível listar o usuário id: " + id
            });
        });
};


exports.update = (req, res) => {
    const id = req.params.id;

    User.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: `Atualizado com sucesso id: ${id}`
                });
            } else {
                res.send({
                    message: `Não foi possível atualizar o cliente id=${id}.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Erro ao atualizar o cliente id=" + id
            });
        });
};

// Excluir todos os clientes do banco de dados
exports.deleteAll = (req, res) => {
    User.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `Todos os usuários foram deletados com sucesso` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Erro ao deletar todos os clientes"
            });
        });
};

// Excluir usuário pelo ID
exports.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: `usuário deletado com sucesso id: ${id}`
                });
            } else {
                res.send({
                    message: `não foi possível deletar usuário id: ${id}. Talvez o usuário não exista!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete cliente with id=" + id
            });
        });
};