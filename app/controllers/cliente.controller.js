const db = require("../models");
const Cliente = db.clientes;
const Op = db.Sequelize.Op;

// Criar e salvar novo cliente
exports.create = (req, res) => {
    // validação request
    if (!req.body.nome) {
        res.status(400).send({
            message: "Error"
        });
        return;
    }

    const cliente = {
        nome: req.body.nome,
        email: req.body.email,
        status: req.body.status ? req.body.status : false
    };

    // Salvar cliente no banco de dados
    Cliente.create(cliente)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Não possível cadastrar cliente."
            });
        });
};

// Listar todos clientes do banco de dados
exports.findAll = (req, res) => {

    const nome = req.query.nome;
    var condition = nome ? { nome: { [Op.iLike]: `%${nome}%` } } : null;

    Cliente.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Não foi possível listar todos os clientes"
            });
        });
};

// Listar clientes pelo ID
exports.findOne = (req, res) => {
    const id = req.params.id;

    Cliente.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Não foi possível listar o cliente id: " + id
            });
        });
};

// Update no cliente pelo ID
exports.update = (req, res) => {
    const id = req.params.id;

    Cliente.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: `Atualizado com sucesso id: ${id}`
                });
            } else {
                res.send({
                    message: `Não foi possível atualizar o cliente id=${id}.`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Erro ao atualizar o cliente id=" + id
            });
        });
};

// Excluir cliente pelo ID
exports.delete = (req, res) => {
    const id = req.params.id;

    Cliente.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: `Cliente deletado com sucesso id: ${id}`
                });
            } else {
                res.send({
                    message: `Cannot delete cliente with id: ${id}. Maybe cliente was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete cliente with id=" + id
            });
        });
};

// Excluir todos os clientes do banco de dados
exports.deleteAll = (req, res) => {
    Cliente.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Todos os clientes foram deletados com sucesso` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Erro ao deletar todos os clientes"
            });
        });
};

// Listar clientes pelo status true
exports.findAllStatusTrue = (req, res) => {
    Cliente.findAll({ where: { status: true } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Não foi possível encontrar clientes"
            });
        });
};

// Listar clientes pelo status false
exports.findAllStatusFalse = (req, res) => {
    Cliente.findAll({ where: { status: false } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Não foi possível encontrar clientes"
            });
        });
};
