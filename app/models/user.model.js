const argon2 = require('argon2');

module.exports = (sequelize, DataType) => {
    const user = sequelize.define("user", {
        id: {
            type: DataType.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: DataType.STRING,
            unique: false,
            allowNull: false
        },
        last_name: {
            type: DataType.STRING,
            unique: false,
            allowNull: false
        },
        birthday: {
            type: DataType.DATE,
            unique: true,
            allowNull: false
        },
        email: {
            type: DataType.STRING,
            unique: false,
            allowNull: false
        },
        fone: {
            type: DataType.STRING,
            unique: false,
            allowNull: false
        },
        user_access: {
            type: DataType.STRING,
            unique: true,
            allowNull: false
        },
        password: {
            type: DataType.STRING,
            unique: false,
            validate: {
                notEmpty: true
            }
        },
    },
        {
            hooks: {

                async beforeCreate(user) {

                    try {

                        const hash = await argon2.hash(user.password);
                        user.set('password', hash);

                    } catch (e) {

                        console.error(err)

                    }
                }
            }
        });

    user.verifyPassword = async (hash, password) => {

        try {

            if (await argon2.verify(hash, password)) {

                return true;
            }

        }
        catch (e) {
            console.error(eerr)
        }

        return false;
    };

    return user;

};