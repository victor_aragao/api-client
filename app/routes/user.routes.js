const users = require("../controllers/user.controller");

var router = require("express").Router();

// Criar e salvar novo usuário
router.post("/user/", users.create);

// Listar todos os usuários
router.get("/user/", users.findAll);

// Atualizar usuário pelo id
router.put("/user/:id", users.update);

// Deletar usuário pelo user
router.delete("/user/:id", users.delete);

// Deletar todos os usuários 
router.delete("/user/", users.deleteAll);


module.exports = router;