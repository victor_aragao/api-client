const clientes = require("../controllers/cliente.controller");

var router = require("express").Router();

// Criar e salvar novo cliente
router.post("/api/", clientes.create);

// Listar todos os clientes
router.get("/api/", clientes.findAll);

// Listar cliente pelo ID
router.get("/api/:id", clientes.findOne);

// Listar clientes pelo status true
router.get("/api/status/true", clientes.findAllStatusTrue);

// Listar clientes pelo status false
router.get("/api/status/false", clientes.findAllStatusFalse);

// Atualizar cliente pelo id 
router.put("/api/:id", clientes.update);

// Deletar cliente pelo id
router.delete("/api/:id", clientes.delete);

// Deletar todos os clientes
router.delete("/api/", clientes.deleteAll);


module.exports = router;