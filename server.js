const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./app/models");
const jwt = require('jsonwebtoken');

const app = express();

var corsOptions = {
    origin: "http://localhost:3000"
};

app.use(express.json());

app.use(cors(corsOptions));

app.use(bodyParser.json());

db.sequelize.sync();


//Remover tabelas existentes

/*
db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});
*/


/*
app.get("/", (req, res) => {
    res.json({
        message: "Bem-vindo a  API Cliente + porta: " + port
    });
});
*/

const routes = require("./app/routes/cliente.routes");
const routes_user = require("./app/routes/user.routes");

app.use(routes);
app.use(routes_user);

app.set('port', (process.env.PORT || 5000));

//For avoidong Heroku $PORT error
app.get('/', function(request, response) {
    var result = 'App is running'
    response.send(result);
}).listen(app.get('port'), function() {
    console.log('App is running, server is listening on port ', app.get('port'));
});

app.use(express.static('public'));