# API Rest Full 

### Banco de dados 

Para iniciar a API, crie um arquivo .env na raiz do diretorio local e coloque as configurações do seu db.
A Api está configurada para utilziar o PostgresSQL.

```
    DB_CONNECTION=postgres
    DB_HOST=localhost
    DB_PORT=5432
    DB_DATABASE=postgres
    DB_USER=postgres
    DB_PASSWORD=postgres
```

### Depêndencias 
Após isso utilize comando para instalar as depêndencias da projeto:

```
    npm install
```

## Rodando a aplicação
Utilize o nodemon para rodar o servidor:

```
    nodemon server.js
```

## Rotas
Este projeto possui integração do back com front-end, porém caso queria fazer requisições manuais
```
POST    = http://localhost:3000/user/ 
GET     = http://localhost:3000/user/
// Deletar pelo id
DELETE  = http://localhost:3000/user/:id 
// Deletar todos os usuários
DELETE  = http://localhost:3000/user/
PUT     = http://localhost:3000/user/

```

## Limpando o banco de dados
No arquivo server.js há uma parte comentada do código
```
db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});
```
Esta parte da aplicação é pra caso você queira remover tabelas existentes do seu banco de dados

## Paramêtros de entrada para requisições
Como pedido pelo desafio, as requisições são assíncronas via Axios, são 5 requisições
    1. (GET) - Listar usuários, como também explícito no desafio, era pra retornar num estilo de tabela os campos, Nome, Sobrenome, Email.
    2. (POST) - Cadastrar usuário, possui um total de 8 campos, todos são obrigatórisos via HTML, além da validação dos campos birthday, email, password.
    3. (DELETE by id) - Esta requisição deleta o usuário de acordo com id passado.
    4. (DELETE all) - Esta requisição diferente da anterior não tem parâmetro, ou seja, ela deleta todos os usuários cadastrados no banco de dados.
    5. (PUT) - Por fim a última requisição é para atualizar os dados de um usuário existente, importante, é preciso escrever em todos os campos para consequir fazer a requisição
    
    
## Tratamento de exceções
A biblioteca Axios, faz a ligação entre back-end e front-end e principalmente fazendo tratamento de exceções, em alguns partes utilizei função alert(), para deixar mais explícito ao usuário o que aconteceu



