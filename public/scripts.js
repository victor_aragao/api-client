/* Request Get */
function listarUsuarios() {
    axios.get('http://localhost:3000/user/')
        .then(response => listaUsuarios(response.data))
        .catch(error => console.log(error))

    const listaUsuarios = (usuarios) => {
        const ulUser = document.getElementById('usuarios')
        usuarios.map(usuario => {
            const listaUser = document.createElement('p')
            listaUser.innerHTML = `${usuario.name}`
            listaUser.className = 'title-data'
            ulUser.appendChild(listaUser)
        })

        const ulSobNome = document.getElementById('sobnome')
        usuarios.map(usuario => {
            const listaSub = document.createElement('p')
            listaSub.innerHTML = `${usuario.last_name}`
            listaSub.className = 'title-data'
            ulSobNome.appendChild(listaSub)
        })

        const ulEmail = document.getElementById('email')
        usuarios.map(usuario => {
            const listaEmail = document.createElement('p')
            listaEmail.innerHTML = `${usuario.email}`
            listaEmail.className = 'title-data'
            ulEmail.appendChild(listaEmail)
        })

    }
}

/*Request Delete All */
function deleteTudo() {
    if (confirm("Tem certeza que deseja excluir todos os usuários?")) {
        axios.delete('http://localhost:3000/user/')
            .then(response => {
                alert(response.data.message);
            })
            .catch(error => console.error(error));
    } else {
        return false;
    }
}
